import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { MoviesService } from '../services/movies.service';
import { Movie } from '../interfaces/movie'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    movies: Movie[];
  constructor(private moviesService: MoviesService) {
    this.moviesService.get().subscribe((data: Movie[]) => {
       this.movies = data;
    }, (error) => {
      console.log(error);
      alert('Ocurrio un error');
    });
   }

  ngOnInit() {
  }
  
  //funcion para eliminar una pelicula por medio de su id

  delete(id){
     this.moviesService.delete(id).subscribe((data) => {
       alert('Eliminado Con exito');
       console.log(data);
     }, (error) =>{
       console.log(error);
     });

  }

}

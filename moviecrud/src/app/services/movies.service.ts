import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Type } from '@angular/core';
import { Movie } from '../interfaces/movie';

@Injectable({
  providedIn: 'root'
})



export class MoviesService {
  API_ENPOINT = 'http://127.0.0.1:8000/api';
  constructor(private httpClient:  HttpClient) { }
  //Me traera las peliculas 
  get(){
   return  this.httpClient.get(this.API_ENPOINT + '/movies');
  }
 // envia los datos de la pelicula para guardar
  save(movie: Movie){
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.httpClient.post(this.API_ENPOINT + '/movies', movie, {headers: headers});
  }
  put(movie){
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.httpClient.put(this.API_ENPOINT + '/movies/' + movie.id, movie, {headers: headers});
  }
  
  delete(id){
    return  this.httpClient.delete(this.API_ENPOINT + '/movies/' + id);
  }

}

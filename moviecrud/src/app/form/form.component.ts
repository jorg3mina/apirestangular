import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Movie } from '../interfaces/movie';
import { MoviesService } from '../services/movies.service';

// encuentras las peliculas seleccionadas con su id
// he inserta nuevas peliculas

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  movie: Movie = {
    name: null,
    description: null,
    genre: null,
    year: null,
    duration: null,
  };

  id: any;
  edit: boolean = false;
  movies: Movie[];
  constructor(private moviesService: MoviesService, private activatedRoute: ActivatedRoute) { 
   this.id = this.activatedRoute.snapshot.params['id'];
   if(this.id){
     this.edit = true;
     this.moviesService.get().subscribe((data: Movie[])=>{
      this.movies = data;
      this.movie = this.movies.find((m) => {return m.id == this.id});
      console.log(this.movie);
     },(error)=>{
       console.log(error);
     });
   }else{
     this.edit = false;
   }
   console.log(this.id);
  }

  ngOnInit() {
  }
//guradas las pelicula y editarlas

  saveMovie(){
    if(this.edit){
      this.moviesService.put(this.movie).subscribe((data) => {
        alert('Pelicula Actualizada');
        console.log(data);
      }, (error) => {
        console.log(error);
        alert('Ocurrio un error');
      }); 
    }else{
    this.moviesService.save(this.movie).subscribe((data) => {
      alert('Pelicula Guardada');
      console.log(data);
    }, (error) => {
      console.log(error);
      alert('Ocurrio un error');
    });
  }
 }
}
